package com.example.urbantour2019;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.widget.Toast;


import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.urbantour2019.restapi.BaseUrl;
import com.example.urbantour2019.restapi.CategoriaInteface;
import com.example.urbantour2019.restapi.CategoriaSitioInterface;
import com.example.urbantour2019.restapi.LineasInterface;
import com.example.urbantour2019.restapi.RetrofitApiAdapter;
import com.example.urbantour2019.restapi.SitioInterface;
import com.example.urbantour2019.ui.Categorias.recicle_favoritos;
import com.example.urbantour2019.ui.Categorias.recicle_lineas;
import com.example.urbantour2019.ui.RoomDatabase.UrbanTourDatabase;
import com.example.urbantour2019.ui.entity.Categoria;
import com.example.urbantour2019.ui.entity.CategoriaSitio;
import com.example.urbantour2019.ui.entity.Linea;
import com.example.urbantour2019.ui.entity.PuntoGeografico;
import com.example.urbantour2019.ui.entity.Sitio;
import com.example.urbantour2019.ui.entity.SitiosCercanosLinea;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements recicle_favoritos.OnFragmentInteractionListener, recicle_lineas.OnFragmentInteractionListener{

    private ProgressDialog progressDialog;

    private AppBarConfiguration mAppBarConfiguration;
    LocationManager locationManager;
    AlertDialog alert = null;
    Location location;

    CategoriaInteface categoriaInteface;
    List<Categoria> categorias;

    CategoriaSitioInterface categoriaSitioInterface;
    List<CategoriaSitio> categoriaSitios;

    SitioInterface sitioInterface;
    List<Sitio> sitios;

    LineasInterface lineasInterface;
    List<Linea> lineas;

    private UrbanTourDatabase appBd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         appBd = UrbanTourDatabase.getInstance(this);//ACA INSTANCIMOS LA BD LOCAL
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Espera mono!");
        progressDialog.setMessage("Cargando la base de datos");
        progressDialog.setCancelable(false);


        /**Carga la base de datos local*/
        cargarBaseDatos();

        /**Dibuja el menu*/
        dibujarMenu();

        /**Me fijo si el GPS esta prendido*/
        locationManagment();
    }

    private void cargarBaseDatos() {

        Log.d("MainActivity", "Voy a invocar a obtenerCategorias");
        obtenerCategorias();


    }

    private void traerSitiosCercanosLinea(int id) {
        sitioInterface = new RetrofitApiAdapter().connectionEnable().create(SitioInterface.class);
        Call<List<Sitio>> call = sitioInterface.getSitiosCercanos(id);
        call.enqueue(new Callback<List<Sitio>>() {
            @Override
            public void onResponse(Call<List<Sitio>> call, Response<List<Sitio>> response) {
                SitiosCercanosLinea sitiosCercanosLinea = new SitiosCercanosLinea();
                if (verificarCodigo(response)) {
                   List<Sitio> sitiosCercanos= response.body();
                   for(Sitio sitio : sitiosCercanos){

                       try {
                           sitiosCercanosLinea.setNroLinea(id);
                           sitiosCercanosLinea.setNombre(sitio.getNombre());
                           sitiosCercanosLinea.setLatitud(sitio.getLatitud());
                           sitiosCercanosLinea.setLongitud(sitio.getLongitud());
                           sitiosCercanosLinea.setDescripcionFinal(sitio.getDescripcionFinal());
                           sitiosCercanosLinea.setDescripcionPrevia(sitio.getDescripcionPrevia());
                           sitiosCercanosLinea.setDisponibilidadHoraria(sitio.getDisponibilidadHoraria());
                           sitiosCercanosLinea.setFavorito(sitio.isFavorito());
                           sitiosCercanosLinea.setImagen(sitio.getImagen());

                           appBd.sitioCercanoLineaDao().insert(sitiosCercanosLinea);

                       } catch (Exception e) {
                           e.printStackTrace();
                           Log.d("Catch  ", e.getMessage());
                       }
                   }
                }
            }
            @Override
            public void onFailure(Call<List<Sitio>> call, Throwable t) {
            }
        });

    }
    //TRAE LA LINEAS Y EL LINESTRING
    public void traerLineas() {

        lineasInterface = new RetrofitApiAdapter().connectionEnable().create(LineasInterface.class);
        Call<List<Linea>> call = lineasInterface.getLineas();
        call.enqueue(new Callback<List<Linea>>() {
            @Override
            public void onResponse(Call<List<Linea>> call, Response<List<Linea>> response) {
              List<Linea> lineas;
                if (verificarCodigo(response)) {
                    lineas = response.body();
                    Log.d("Body","Esto es el body "+response.body());
                    //
                    for (Linea lineaAux : lineas) {
                        try {
                            //INSERTAS LA LINEA
                            appBd.lineaDao().insert(lineaAux);
                            for(PuntoGeografico puntoGeografico : lineaAux.getLocation()){
                                PuntoGeografico puntoGeograficoAux = new PuntoGeografico();
                                puntoGeograficoAux.setLatitud(puntoGeografico.getLatitud());
                                puntoGeograficoAux.setLongitud(puntoGeografico.getLongitud());
                                puntoGeograficoAux.setLinea_id(lineaAux.getId());
                                appBd.puntoGeograficoDao().insert(puntoGeograficoAux);//ACA INSERTAMOS A LA BD LOCAL UN NUEVO OBJETO
                            }
                            Log.d("Try insert","Inserte "+ lineaAux.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("Catch  ", e.getMessage());
                        }
                        Log.d("MainActivity", "Voy a invocar a traerSitiosCercanosLinea");
                        traerSitiosCercanosLinea(lineaAux.getId());
                    }

                }
                Log.d("MainActivity", "Voy a invocar a traerSitios");
                traerSitios();
            }

            @Override
            public void onFailure(Call<List<Linea>> call, Throwable t) {
                lineas = appBd.lineaDao().getAllLineas();
                progressDialog.cancel();
            }
        });
    }

    public void dibujarMenu() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,R.id.monumentosFragment, R.id.muralesFragment, R.id.museoFragment
        , R.id.teatroFragment, R.id.comedorFragment, R.id.recicle_favoritos, R.id.recicle_lineas).setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void locationManagment() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertNoGps();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            } else {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
              }
        } else {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
    }

    private void AlertNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El sistema GPS esta desactivado, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void obtenerCategorias() {
        progressDialog.show();

        categoriaInteface = new RetrofitApiAdapter().connectionEnable().create(CategoriaInteface.class);
        Call<List<Categoria>> call = categoriaInteface.getCategorias();
        call.enqueue(new Callback<List<Categoria>>() {
            @Override
            public void onResponse(Call<List<Categoria>> call, Response<List<Categoria>> response) {

                List<Categoria> categorias;

                if (verificarCodigo(response)) {
                    categorias = response.body();
                    for (Categoria categoria : categorias) {
                        try {
                            appBd.categoriaDao().insertCategoria(categoria);
                            Log.d("Try insert","Inserte "+ categoria.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("Catch  ", e.getMessage());
                        }
                    }
                }
                Log.d("MainActivity", "Voy a invocar a traerLineas");
                traerLineas();
            }
            @Override
            public void onFailure(Call<List<Categoria>> call, Throwable t) {
                categorias = appBd.categoriaDao().getCategoriasList();
                progressDialog.cancel();
            }
        });
    }

    private void obtenerSitiosPorCategorias() {
        categoriaSitioInterface = new RetrofitApiAdapter().connectionEnable().create(CategoriaSitioInterface.class);
        Call<List<CategoriaSitio>> call = categoriaSitioInterface.getCategoriasSitios();
        call.enqueue(new Callback<List<CategoriaSitio>>() {
            @Override
            public void onResponse(Call<List<CategoriaSitio>> call, Response<List<CategoriaSitio>> response) {

                List<CategoriaSitio> categoriaSitios;

                if (verificarCodigo(response)) {
                    categoriaSitios = response.body();
                    for (CategoriaSitio categoriaSitio : categoriaSitios) {
                        try {
                            appBd.categoriaSitioDao().insert(categoriaSitio);
                            Log.d("Try insert","Inserte "+ categoriaSitio.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("Catch  ", e.getMessage());
                        }
                    }
                }
               progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<List<CategoriaSitio>> call, Throwable t) {
                Log.d("onFailure", "Se pudrio con el server pero traigo los datos de la bd" +
                        " local mono!");
                categoriaSitios = appBd.categoriaSitioDao().getAllCategoriaSitios();
                progressDialog.cancel();
            }
        });
    }

    private boolean verificarCodigo(Response response) {
        Log.d("Main","codigo de response "+ response.code());
        return response.code() == 200 ? true : false;
    }

    private void traerSitios() {
        sitioInterface = new RetrofitApiAdapter().connectionEnable().create(SitioInterface.class);
        Call<List<Sitio>> call = sitioInterface.getSitios();
        call.enqueue(new Callback<List<Sitio>>() {
            @Override
            public void onResponse(Call<List<Sitio>> call, Response<List<Sitio>> response) {

                List<Sitio> sitiosAux;

                URL imageUrl=null;

                if (verificarCodigo(response)) {
                    sitiosAux = response.body();
                    for (Sitio sitio : sitiosAux) {
                        try {

               /*             imageUrl = new URL(BaseUrl.URL_WEB_IMAGES + sitio.getImagen());
                            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
                            conn.connect();
                            Bitmap loadedImage = BitmapFactory.decodeStream(conn.getInputStream());

                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            loadedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();
                            loadedImage.recycle();*/



                            appBd.sitioDao().insertSitio(sitio);
                            Log.d("Try insert","Inserte "+ sitio.toString());


                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("Catch  ", e.getMessage());
                        }
                    }
                }

                Log.d("MainActivity", "Voy a invocar a obtenerSitiosPorCategorias");
                obtenerSitiosPorCategorias();
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<List<Sitio>> call, Throwable t) {
                Log.d("onFailure", "Se pudrio con el server pero traigo los datos de la bd" +
                        " local mono!");
                sitios = appBd.sitioDao().getSitioList();
                Toast toast1 = Toast.makeText(getApplicationContext(), "Tratado de usar la bdLocal", Toast.LENGTH_SHORT);
                toast1.show();
                progressDialog.cancel();
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}