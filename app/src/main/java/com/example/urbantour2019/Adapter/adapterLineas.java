package com.example.urbantour2019.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.urbantour2019.R;
import com.example.urbantour2019.ui.entity.Linea;
import com.example.urbantour2019.ui.entity.Sitio;

import java.util.List;

public class adapterLineas extends RecyclerView.Adapter<adapterLineas.Holder> implements View.OnClickListener {

    private List<Linea> lineas;
    private View.OnClickListener listener;
    private Context context;


    public adapterLineas(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        if (this.listener!=null)
            listener.onClick(v);
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setLineas(List<Linea> lineas) {
        Log.d("ADAPTER" + " ZITIOS", lineas.toString());
        this.lineas = lineas;
    }


    @NonNull//INFLAMOS LA VISTA CON EL XML layout_favoritos(ESTA LA FORMA QUE TIENE NUESTRO DATO)
    @Override
    public adapterLineas.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(this.context).inflate(R.layout.layout_lineas, null);
        vista.setOnClickListener(this);
        return new Holder(vista);
    }

    @Override//COMUNICA ENTRE NUESTRO ADAPTADOR Y LA CLASE HOLDER
    public void onBindViewHolder(@NonNull adapterLineas.Holder holder, int position) {
        try {
            Linea linea = this.lineas.get(position);
            Log.d("PANCHOO" + " SOY UN SITIO", lineas.toString());

            holder.nombreLinea.setText(linea.getNombre());
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        if (this.lineas!=null)
            return this.lineas.size();
        return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView nombreLinea;

        public Holder(@NonNull View itemView) {
            super(itemView);
            nombreLinea=itemView.findViewById(R.id.lineId);
        }
/*
        public void asignarDatos(Sitio sitio) {
            nombreFavorito.setText(sitio.getNombre());
            descripcionFavorito.setText((sitio.getDescripcionPrevia()));
        }*/
    }
}
