package com.example.urbantour2019.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.urbantour2019.R;
import com.example.urbantour2019.ui.entity.Sitio;

import java.util.List;

public class adapterFavoritos extends RecyclerView.Adapter<adapterFavoritos.Holder> implements View.OnClickListener {

    private List<Sitio> sitiosFavoritos;
    private View.OnClickListener listener;
    private Context context;


    public adapterFavoritos(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View v) {
    if (this.listener!=null)
        listener.onClick(v);
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setSitiosFavoritos(List<Sitio> sitiosFavoritos) {
        Log.d("ADAPTER" + " ZITIOS", sitiosFavoritos.toString());
        this.sitiosFavoritos = sitiosFavoritos;
    }


    @NonNull//INFLAMOS LA VISTA CON EL XML layout_favoritos(ESTA LA FORMA QUE TIENE NUESTRO DATO)
    @Override
    public adapterFavoritos.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(this.context).inflate(R.layout.layout_favoritos, null);
        vista.setOnClickListener(this);
        return new Holder(vista);
    }

    @Override//COMUNICA ENTRE NUESTRO ADAPTADOR Y LA CLASE HOLDER
    public void onBindViewHolder(@NonNull adapterFavoritos.Holder holder, int position) {
        try {
            Sitio sitio = this.sitiosFavoritos.get(position);
        Log.d("PANCHOO" + " SOY UN SITIO", sitiosFavoritos.toString());

        holder.nombreFavorito.setText(sitio.getNombre());
        holder.descripcionFavorito.setText(sitio.getDescripcionPrevia());
        //holder.foto.setImageDrawable(sitio.getImagen());
        } catch (Exception e){
            e.printStackTrace();
        }
    }


        @Override
    public int getItemCount() {
        if (this.sitiosFavoritos!=null)
            return this.sitiosFavoritos.size();
        return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView nombreFavorito, descripcionFavorito;
        ImageView foto;

        public Holder(@NonNull View itemView) {
            super(itemView);
            nombreFavorito=itemView.findViewById(R.id.nombreSitioFavorito);
            descripcionFavorito=itemView.findViewById(R.id.informacionSitioFavorito);
            //foto=itemView.findViewById(R.id.imgSitioFavorito);
        }
/*
        public void asignarDatos(Sitio sitio) {
            nombreFavorito.setText(sitio.getNombre());
            descripcionFavorito.setText((sitio.getDescripcionPrevia()));
        }*/
    }
}
