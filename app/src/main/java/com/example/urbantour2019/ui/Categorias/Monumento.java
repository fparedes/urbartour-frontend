package com.example.urbantour2019.ui.Categorias;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.urbantour2019.restapi.RetrofitApiAdapter;
import com.example.urbantour2019.restapi.SitioInterface;
import com.example.urbantour2019.ui.Informacion.InformacionSitio;
import com.example.urbantour2019.ui.RoomDatabase.UrbanTourDatabase;
import com.example.urbantour2019.ui.entity.Sitio;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;
import static androidx.core.content.ContextCompat.checkSelfPermission;



public class Monumento extends SupportMapFragment implements OnMapReadyCallback {
    GoogleMap mMap;

    SitioInterface sitioInterface;

    List<Sitio> sitios = new ArrayList<>();

    //Referencia al marcador con la posicion actual del usuario
    private Marker userPos;

    private static final long MIN_TIEMPO_ENTRE_UPDATES = 1000 * 60; // 1 minuto
    //Minima distancia para updates en metros.
    private static final long MIN_CAMBIO_DISTANCIA_PARA_UPDATES = 2; // 2 metros
    //Radio de los geofences
    private static final long GEOFENCE_RADIUS_IN_METERS = 50; // 50 metros

    //Referencia al cliente de Location
    private FusedLocationProviderClient fusedLocationClient;

    UrbanTourDatabase appBd;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        appBd = UrbanTourDatabase.getInstance(getContext());
        getMapAsync(this);

        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Setear posicion actual del usuario en el mapa
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Log.d("prie", "onLocationChanged: Se movio el usuario!!" + location.getLatitude() + location.getLongitude());
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            //LatLng latLng = new LatLng(-42.7779659, -65.0258803);
                            // MarkerOptions aux = new MarkerOptions().position(latLng).title("Usted esta aqui!");
                            //userPos = mMap.addMarker(aux);
                            mMap.setMyLocationEnabled(true);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                            // Toast toast = Toast.makeText(getContext(), "onSuccess", Toast.LENGTH_LONG);
                            //toast.show();
                        }
                    }
                });

        Log.i("prueba", "onMapReady: Ubicacion del usuario Seteada");

        //Setear metodo de cambio de ubicacion del usuario, para mover el marker con el movimiento real
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (userPos != null) {
                    userPos.remove();
                }
                try {
                    Log.d("Position", "onLocationChanged: Se movio el usuario!!" + location.getLatitude() + location.getLongitude());
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.setMyLocationEnabled(true);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                    // ACA SE HACE LA LLAMADA AL MAPA PARA LA NOTIFICACION
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        Log.d("Position", "Manager creado");

        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIEMPO_ENTRE_UPDATES, MIN_CAMBIO_DISTANCIA_PARA_UPDATES, locationListener);
            Log.d("Position", "Registrado por updates de movimiento");
        } catch (SecurityException e) {
            e.printStackTrace();
            Log.d("Position", "No me pude registrar a los updates");
        }

        sitios = appBd.categoriaSitioDao().getSitiosCategorias(1);

        agregarSitiosAlMapa(sitios);
        mMap.setOnMarkerClickListener(this::onMarkerClick);
    }

    private void agregarSitiosAlMapa(List<Sitio> sitios) {

        if (sitios.size() != 0) {
            Log.d("HomeFragment", " antes del for!");
            for (Sitio s : sitios) {
                LatLng latLng = new LatLng(s.getLatitud(), s.getLongitud());
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title(s.getNombre()));
                Log.d("HomeFragment", " Agregado!");
            }
        }

    }

    public boolean onMarkerClick(Marker marker) {
        Intent intent = new Intent(getContext(), InformacionSitio.class);
        Bundle bundle = new Bundle();
        Toast toast1 = Toast.makeText(getContext(), "Redirigido a: " + marker.getTitle(), Toast.LENGTH_SHORT);
        toast1.show();

        Sitio sitio = appBd.sitioDao().getSitioInteres(marker.getTitle());

        Log.d("Sitio", "lo traigo en el marker"+ sitio.toString());

        bundle.putSerializable("nombre", sitio.getNombre());
        bundle.putSerializable("descripcionFinal",sitio.getDescripcionFinal());
        bundle.putSerializable("disponibilidadHoraria",sitio.getDisponibilidadHoraria());

        intent.putExtras(bundle);

        startActivity(intent);

        return false;
    }


}