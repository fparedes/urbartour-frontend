package com.example.urbantour2019.ui.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "lineas",indices = {@Index(value = {"id"},unique = true)})
public class Linea implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "nombre")
    private String nombre;
    @Ignore
    private List<PuntoGeografico> location;

    public Linea(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public List<PuntoGeografico> getLocation() {
        return location;
    }

    public void setLocation(List<PuntoGeografico> location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Linea{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", location(='" + location + '\'' +
                '}';
    }
}