package com.example.urbantour2019.ui.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.urbantour2019.ui.entity.PuntoGeografico;

import java.util.List;

@Dao
public interface PuntoGeograficoDao {

    @Insert
    void insert(PuntoGeografico puntoGeografico);

    @Query("SELECT * FROM puntogeografico WHERE linea_id=:lineaId")
    List<PuntoGeografico> getRecorridoLinea(int lineaId);

}
