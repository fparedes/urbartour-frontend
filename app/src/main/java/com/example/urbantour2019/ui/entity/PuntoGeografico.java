package com.example.urbantour2019.ui.entity;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Linea.class,
        parentColumns = "id",
        childColumns = "linea_id",
        onDelete = CASCADE),
        indices = @Index(value = {"linea_id","id"},unique = false)
)

public class PuntoGeografico {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int linea_id;
    private double latitud;
    private double longitud;

    public PuntoGeografico() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLinea_id() {
        return linea_id;
    }

    public void setLinea_id(int linea_id) {
        this.linea_id = linea_id;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "PuntoGeografico{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }
}
