package com.example.urbantour2019.ui.RoomDatabase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.urbantour2019.ui.Daos.CategoriaDao;
import com.example.urbantour2019.ui.Daos.CategoriaSitioDao;
import com.example.urbantour2019.ui.Daos.LineaDao;
import com.example.urbantour2019.ui.Daos.PuntoGeograficoDao;
import com.example.urbantour2019.ui.Daos.SitioCercanoLineaDao;
import com.example.urbantour2019.ui.Daos.SitioDao;
import com.example.urbantour2019.ui.entity.Categoria;
import com.example.urbantour2019.ui.entity.CategoriaSitio;
import com.example.urbantour2019.ui.entity.Linea;
import com.example.urbantour2019.ui.entity.PuntoGeografico;
import com.example.urbantour2019.ui.entity.Sitio;
import com.example.urbantour2019.ui.entity.SitiosCercanosLinea;

@Database(entities = {Sitio.class, Categoria.class, CategoriaSitio.class, Linea.class,
        PuntoGeografico.class, SitiosCercanosLinea.class},exportSchema = false,version = 1)
public abstract class UrbanTourDatabase extends RoomDatabase {

    private static final String DB_NAME = "urbantour_db";
    private static UrbanTourDatabase instance;
    public static synchronized UrbanTourDatabase getInstance(Context context){

        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    UrbanTourDatabase.class,DB_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }


    public abstract SitioDao sitioDao();
    public abstract CategoriaDao categoriaDao();
    public abstract CategoriaSitioDao categoriaSitioDao();
    public abstract LineaDao lineaDao();
    public abstract PuntoGeograficoDao puntoGeograficoDao();
    public abstract SitioCercanoLineaDao sitioCercanoLineaDao();

}
