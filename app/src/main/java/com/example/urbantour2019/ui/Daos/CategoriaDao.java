package com.example.urbantour2019.ui.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.urbantour2019.ui.entity.Categoria;
import java.util.List;

@Dao
public interface CategoriaDao {

    @Query("Select * from categorias")
    List<Categoria> getCategoriasList();

    @Insert
    void insertCategoria(Categoria categoria);

}
