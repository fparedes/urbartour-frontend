package com.example.urbantour2019.ui.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.urbantour2019.ui.entity.Sitio;

import java.util.List;

//ESTO ES PURO ROOM
@Dao
public interface SitioDao {

    @Query("Select * from sitio")
    List<Sitio> getSitioList();

    @Insert
    void insertSitio(Sitio sitio);

    @Query("select * from sitio where nombre = :nombre_sitio")
    Sitio getSitioInteres(String nombre_sitio);

    @Update
    void updateSitio(Sitio sitio);

    @Query("select * from sitio where sitio.favorito = 1")
    List<Sitio> getSitiosFavoritos();


}
