package com.example.urbantour2019.ui.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.urbantour2019.restapi.SitioInterface;
import com.example.urbantour2019.ui.Informacion.InformacionSitio;
import com.example.urbantour2019.ui.RoomDatabase.UrbanTourDatabase;
import com.example.urbantour2019.ui.entity.Linea;
import com.example.urbantour2019.ui.entity.PuntoGeografico;
import com.example.urbantour2019.ui.entity.Sitio;
import com.example.urbantour2019.ui.entity.SitiosCercanosLinea;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;
import static androidx.core.content.ContextCompat.checkSelfPermission;


public class HomeFragment extends SupportMapFragment implements OnMapReadyCallback/* GoogleMap.OnMarkerClickListener */ {
    GoogleMap mMap;

    UrbanTourDatabase appBd;
    //List<Sitio> sitios = appBd.sitioDao().getSitioList();
    //Referencia al marcador con la posicion actual del usuario
    private Marker userPos;

    private static final long MIN_TIEMPO_ENTRE_UPDATES = 1000 * 60; // 1 minuto
    //Minima distancia para updates en metros.
    private static final long MIN_CAMBIO_DISTANCIA_PARA_UPDATES = 2; // 2 metros
    //Radio de los geofences
    private static final long GEOFENCE_RADIUS_IN_METERS = 50; // 50 metros

    //Referencia al cliente de Location
    private FusedLocationProviderClient fusedLocationClient;
    private List<LatLng> puntosDeVerdad = null;
    private List<SitiosCercanosLinea> sitiosLineas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        appBd = UrbanTourDatabase.getInstance(getContext());

        Bundle aux = getArguments();
        if(aux != null){
            int nroLinea = (int) aux.get("linea");

            sitiosLineas = appBd.sitioCercanoLineaDao().getSitioLinea(nroLinea+1);
            Log.d("SITIOSLINEAS", "SITIOSLINEAS" + sitiosLineas.toString());

            List<PuntoGeografico> puntos = (List) aux.get("puntos");
            puntosDeVerdad = new ArrayList<LatLng>();
            for(PuntoGeografico p : puntos){
                puntosDeVerdad.add(new LatLng(p.getLatitud(),p.getLongitud()));
            }
        }
        getMapAsync(this);
        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        List<Sitio> sitios = appBd.sitioDao().getSitioList();
        //Setear posicion actual del usuario en el mapa
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Log.d("prie", "onLocationChanged: Se movio el usuario!!" + location.getLatitude() + location.getLongitude());
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.setMyLocationEnabled(true);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                        }
                    }
                });

        Log.i("prueba", "onMapReady: Ubicacion del usuario Seteada");

        //Setear metodo de cambio de ubicacion del usuario, para mover el marker con el movimiento real
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (userPos != null) {
                    userPos.remove();
                }
                try {
                    Log.d("Position", "onLocationChanged: Se movio el usuario!!" + location.getLatitude() + location.getLongitude());
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.setMyLocationEnabled(true);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                   /*TODO query para traer si hay un sitio cerca*/
         /*           if (sitios.size() != 0) {
                        for (Sitio s : sitios) {
                            LatLng l = new LatLng (s.getLatitud(), s.getLongitud());

                            Location.distanceBetween(latLng.latitude, latLng.longitude, l.latitude, l.longitude, 150);
                        }
                    }
*/
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        Log.d("Position", "Manager creado");

        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIEMPO_ENTRE_UPDATES, MIN_CAMBIO_DISTANCIA_PARA_UPDATES, locationListener);
            Log.d("Position", "Registrado por updates de movimiento");
        } catch (SecurityException e) {
            e.printStackTrace();
            Log.d("Position", "No me pude registrar a los updates");
        }

       // List<Sitio> sitios = appBd.sitioDao().getSitioList();
        Log.d("HomeFragmento", "sitios: "+sitios.toString());


        if(puntosDeVerdad != null){

            Polyline poly = googleMap.addPolyline(new PolylineOptions().clickable(false).addAll(puntosDeVerdad));
            //Mostrar sitios cercanos
            agregarSitiosAlMapabyLinea(sitiosLineas);

            mMap.setOnMarkerClickListener(this::onMarkerClick);
            //Log.d("SITIOSLINEAS", "ESTOYCASIADENTROO" + sitiosLineas.toString());

        }else{
            agregarSitiosAlMapa(sitios);
        }
        mMap.setOnMarkerClickListener(this::onMarkerClick);
    }


    private void agregarSitiosAlMapa(List<Sitio> sitios) {

        if (sitios.size() != 0) {
            Log.d("HomeFragment", " antes del for!");
            for (Sitio s : sitios) {
                LatLng latLng = new LatLng(s.getLatitud(), s.getLongitud());
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title(s.getNombre()));
                Log.d("HomeFragment", " Agregado!");
            }
        }

    }

    private void agregarSitiosAlMapabyLinea(List<SitiosCercanosLinea> sitiosLineas) {
            if (sitiosLineas.size() != 0) {
                Log.d("HomeFragment", " antes del for!");
                for (SitiosCercanosLinea s : sitiosLineas) {
                    LatLng latLng = new LatLng(s.getLatitud(), s.getLongitud());
                    mMap.addMarker(new MarkerOptions().position(latLng)
                            .title(s.getNombre()));
                    Log.d("HomeFragment", " Agregado!");
                }
            }
    }


    public boolean onMarkerClick(Marker marker) {
        Intent intent = new Intent(getContext(), InformacionSitio.class);
        Bundle bundle = new Bundle();
        Toast toast1 = Toast.makeText(getContext(), "Redirigido a: " + marker.getTitle(), Toast.LENGTH_SHORT);
        toast1.show();

        Sitio sitio = appBd.sitioDao().getSitioInteres(marker.getTitle());

        Log.d("Sitio", "lo traigo en el marker"+ sitio.toString());

        bundle.putSerializable("nombre", sitio.getNombre());
        bundle.putSerializable("descripcionFinal",sitio.getDescripcionFinal());
        bundle.putSerializable("disponibilidadHoraria",sitio.getDisponibilidadHoraria());
        bundle.putSerializable("imagen", sitio.getImagen());

        intent.putExtras(bundle);

        startActivity(intent);

        return false;
    }

    public void pintar(){

    }

}


