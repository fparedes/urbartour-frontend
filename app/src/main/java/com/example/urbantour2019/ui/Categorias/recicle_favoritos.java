package com.example.urbantour2019.ui.Categorias;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Toast;

import com.example.urbantour2019.Adapter.adapterFavoritos;
import com.example.urbantour2019.R;
import com.example.urbantour2019.ui.Informacion.InformacionSitio;
import com.example.urbantour2019.ui.RoomDatabase.UrbanTourDatabase;
import com.example.urbantour2019.ui.entity.Sitio;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link recicle_favoritos.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link recicle_favoritos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class recicle_favoritos extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private View vista;

    UrbanTourDatabase appBd;
    private List<Sitio> sitios;
    private RecyclerView.LayoutManager manager;
    private RecyclerView recyclerView;

    private adapterFavoritos adapter;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Object RecyclerView;

    public recicle_favoritos() {
        // Required empty public constructor


    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment recicle_favoritos.
     */
    // TODO: Rename and change types and number of parameters
    public static recicle_favoritos newInstance(String param1, String param2) {
        recicle_favoritos fragment = new recicle_favoritos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista =inflater.inflate(R.layout.fragment_recicle_favoritos, container, false);


        appBd = UrbanTourDatabase.getInstance(getContext());

        List<Sitio> sitios = appBd.sitioDao().getSitiosFavoritos();
        Log.d("RecicleFavoritos", "FAVORITOS: "+sitios.toString());


        recyclerView = (RecyclerView)vista.findViewById(R.id.recicleFavoritos);
        manager = new LinearLayoutManager(vista.getContext());
        recyclerView.setLayoutManager(manager);

        adapter = new adapterFavoritos(vista.getContext());

        adapter.setSitiosFavoritos(sitios);
        recyclerView.setAdapter(adapter);

        adapter.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), InformacionSitio.class);
                Bundle bundle = new Bundle();

                Sitio sitio = sitios.get(recyclerView.getChildAdapterPosition(v));
                Log.d("Sitio", "lo traigo en el marker"+ sitio.toString());

                bundle.putSerializable("nombre", sitio.getNombre());
                bundle.putSerializable("descripcionFinal",sitio.getDescripcionFinal());
                bundle.putSerializable("disponibilidadHoraria",sitio.getDisponibilidadHoraria());
                bundle.putSerializable("favorito", sitio.isFavorito());

                intent.putExtras(bundle);

                startActivity(intent);

            }
        });

        return vista;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
