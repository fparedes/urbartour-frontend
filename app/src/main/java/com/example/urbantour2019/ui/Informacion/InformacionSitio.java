package com.example.urbantour2019.ui.Informacion;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.urbantour2019.R;
import com.example.urbantour2019.restapi.BaseUrl;
import com.example.urbantour2019.ui.RoomDatabase.UrbanTourDatabase;
import com.example.urbantour2019.ui.entity.Sitio;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class InformacionSitio extends Activity {
    UrbanTourDatabase appBd;
    TextView nombreSitio, informacionSitio, disponibilidadHoraria;
    boolean favorito;
    Button agregarAFavoritos;
    ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informacion_sitio_layout);

        appBd = UrbanTourDatabase.getInstance(getApplicationContext());
        nombreSitio = findViewById(R.id.nombreSitio);
        informacionSitio = findViewById(R.id.informacionSitio);
        disponibilidadHoraria = findViewById(R.id.disponibilidadHoraria);
        agregarAFavoritos = findViewById(R.id.agregarAFav);
        imageView = findViewById(R.id.imagenId);

        Bundle sitioR = getIntent().getExtras();

        Log.d("MENSAJE " , " SITIOSRRRR" + sitioR.getString("imagen"));

        nombreSitio.setText(sitioR.getString("nombre"));
        informacionSitio.setText(sitioR.getString("descripcionFinal"));
        disponibilidadHoraria.setText(sitioR.getString("disponibilidadHoraria"));

        //ejecuta tarea en segundo plano
        new imagenTask().execute(sitioR.getString("imagen"));

        try{

            favorito=sitioR.getBoolean("favorito");

            if(favorito){
                agregarAFavoritos.setVisibility(View.INVISIBLE);
            } else{
                agregarAFavoritos.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        agregarAFavoritos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Sitio sitio = new Sitio();
                sitio = appBd.sitioDao().getSitioInteres(sitioR.getString("nombre"));

                Log.d("info", "sitio " + sitio.toString());

                sitio.setFavorito(true);

                appBd.sitioDao().updateSitio(sitio);

                Log.d("info", "sitio " + sitio.toString());

                Toast toast1 = Toast.makeText(getApplicationContext(), "Agreaste " + sitio.getNombre()+" a favoritos", Toast.LENGTH_SHORT);
                toast1.show();


            }
        });

    }

    class imagenTask extends AsyncTask<String, Void, Void> {

        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(String... imagen) {
            try{

                URL imageUrl = new URL(BaseUrl.URL_WEB_IMAGES + imagen[0]);
                Log.d("ENTRE AL CATCH DE URL" , " " + imageUrl.toString());

                HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
                conn.connect();
                Bitmap loadedImage = BitmapFactory.decodeStream(conn.getInputStream());
                imageView.setImageBitmap(loadedImage);


                // ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // loadedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                //byte[] byteArray = stream.toByteArray();
                //sloadedImage.recycle();

            }catch(Exception e) {
                e.printStackTrace();
            }
        return null;
        }
    }


}

