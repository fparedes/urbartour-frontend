package com.example.urbantour2019.ui.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.urbantour2019.ui.entity.Sitio;
import com.example.urbantour2019.ui.entity.SitiosCercanosLinea;

import java.util.List;

@Dao
public interface SitioCercanoLineaDao {

    @Insert
    void insert(SitiosCercanosLinea sitiosCercanosLinea);

    @Query("SELECT * FROM sitio_cercano_linea WHERE nroLinea=:nroLinea")
    List<SitiosCercanosLinea> getSitioLinea(int nroLinea);


}
