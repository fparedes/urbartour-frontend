package com.example.urbantour2019.ui.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "sitio_cercano_linea",
        indices = {@Index(value = {"nombre", "latitud","longitud"},
                unique = true)})
public class SitiosCercanosLinea {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "nombre")
    private String nombre;
    @ColumnInfo(name = "descripcionPrevia")
    private String descripcionPrevia;
    @ColumnInfo(name = "descripcionFinal")
    private String descripcionFinal;
    @ColumnInfo(name = "disponibilidadHoraria")
    private String disponibilidadHoraria;
    @ColumnInfo(name = "latitud")
    private double latitud;
    @ColumnInfo(name = "longitud")
    private double longitud;
    @ColumnInfo(name = "imagen")
    private String imagen;
    @ColumnInfo(name = "favorito")
    private boolean favorito;
    @ColumnInfo(name = "nroLinea")
    private int nroLinea;

    public SitiosCercanosLinea() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcionPrevia() {
        return descripcionPrevia;
    }

    public void setDescripcionPrevia(String descripcionPrevia) {
        this.descripcionPrevia = descripcionPrevia;
    }

    public String getDescripcionFinal() {
        return descripcionFinal;
    }

    public void setDescripcionFinal(String descripcionFinal) {
        this.descripcionFinal = descripcionFinal;
    }

    public String getDisponibilidadHoraria() {
        return disponibilidadHoraria;
    }

    public void setDisponibilidadHoraria(String disponibilidadHoraria) {
        this.disponibilidadHoraria = disponibilidadHoraria;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public int getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(int nroLinea) {
        this.nroLinea = nroLinea;
    }

    @Override
    public String toString() {
        return "SitiosCercanosLinea{" +
                ", nombre='" + nombre + '\'' +
                ", nroLinea=" + nroLinea +
                '}';
    }

}
