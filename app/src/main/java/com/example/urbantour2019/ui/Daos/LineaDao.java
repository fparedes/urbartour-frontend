package com.example.urbantour2019.ui.Daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.urbantour2019.ui.entity.Linea;
import com.example.urbantour2019.ui.entity.PuntoGeografico;

import java.util.List;

@Dao
public interface LineaDao {

    @Query("Select * from lineas")
    List<Linea> getAllLineas();

    @Query("Select * from PuntoGeografico where linea_id=:aux")
    List<PuntoGeografico> getAllLineasById(int aux);

    @Insert
    void insert(Linea linea);

    @Update
    void update(Linea linea);

    @Delete
    void delete(Linea linea);
}
