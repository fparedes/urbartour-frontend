package com.example.urbantour2019.ui.Daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.urbantour2019.ui.entity.CategoriaSitio;
import com.example.urbantour2019.ui.entity.Sitio;


import java.util.List;

@Dao
public interface CategoriaSitioDao {

    @Query("Select * from categoria_sitio")
    List<CategoriaSitio> getAllCategoriaSitios();

    @Insert
    void insert(CategoriaSitio categoriaSitio);
/*
    @Query("SELECT * FROM categorias " +
            "INNER JOIN categoria_sitio " +
            "ON categorias.id=categoria_sitio.categoria_id " +
            "WHERE categoria_sitio.categoria_id=:catId")
    List<Categoria> getCategoriasSitio(int catId);

*/
    @Query("SELECT * FROM sitio " +
            "INNER JOIN categoria_sitio " +
            "ON sitio.id=categoria_sitio.sitio_id " +
            "WHERE categoria_sitio.categoria_id=:sitioId")
    List<Sitio> getSitiosCategorias(int sitioId);



}
