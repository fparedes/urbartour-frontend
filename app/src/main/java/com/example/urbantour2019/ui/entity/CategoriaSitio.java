package com.example.urbantour2019.ui.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import java.io.Serializable;

@Entity(tableName = "categoria_sitio",
primaryKeys = {"categoria_id","sitio_id"},
foreignKeys = {@ForeignKey(entity = Categoria.class,
                            parentColumns = "id",
                            childColumns = "categoria_id"),
                @ForeignKey(entity = Sitio.class,
                            parentColumns = "id",
                            childColumns = "sitio_id")
},
indices = {@Index(value = {"sitio_id"},unique = false)})
public class CategoriaSitio implements Serializable {

    @ColumnInfo(name = "categoria_id")
    private int categoria_id;
    @ColumnInfo(name = "sitio_id")
    private int sitio_id;

    public CategoriaSitio() {
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public int getSitio_id() {
        return sitio_id;
    }

    public void setSitio_id(int sitio_id) {
        this.sitio_id = sitio_id;
    }

    @Override
    public String toString() {
        return "CategoriaSitio{" +
                "categoria_id=" + categoria_id +
                ", sitio_id=" + sitio_id +
                '}';
    }
}
