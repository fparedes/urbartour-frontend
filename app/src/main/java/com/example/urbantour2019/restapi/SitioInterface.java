package com.example.urbantour2019.restapi;

import com.example.urbantour2019.ui.entity.Sitio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SitioInterface {
    @GET("sitios")
    Call<List<Sitio>> getSitios();

    @GET("categoria_sitio/findSitio/{id}")
    Call<List<Sitio>> getSitiosById(@Path("id") int id);

    @GET("linea_sitio/sitiosCercanos/{id}")
    Call<List<Sitio>> getSitiosCercanos(@Path("id") int id);

}
