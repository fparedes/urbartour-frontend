package com.example.urbantour2019.restapi;

import com.example.urbantour2019.ui.entity.Sitio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CategoriaInterface {


    /**Trae los sitios dependiendo la categoria*/
    @GET("categoria_sitio/findSitio/{id}")
    Call<List<Sitio>> findSitio(@Path("id") int id);


    /**Trae todos los sitios de interes que estan en la base de datos*/
   // @GET("sitios")
    //Call<List<SitioViewModel>> getSitios();

    //@GET("categorias")
    //Call<List<CategoriasViewModel>> getCategorias();

    //@GET("sitio_categoria/findCategoria/{id}")
    //Call<List<CategoriasViewModel>> getSitiosPorCategoria(@Path("id") int id);
}
