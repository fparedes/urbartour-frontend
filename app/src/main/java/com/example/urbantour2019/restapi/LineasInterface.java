package com.example.urbantour2019.restapi;

import com.example.urbantour2019.ui.entity.Linea;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LineasInterface {

    @GET("lineas")
    Call<List<Linea>> getLineas();

}
