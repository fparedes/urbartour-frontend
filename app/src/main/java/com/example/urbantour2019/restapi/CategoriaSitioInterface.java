package com.example.urbantour2019.restapi;

import com.example.urbantour2019.ui.entity.CategoriaSitio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriaSitioInterface {

    @GET("categoria_sitio/categoriasSitios")
    Call<List<CategoriaSitio>> getCategoriasSitios();

}
