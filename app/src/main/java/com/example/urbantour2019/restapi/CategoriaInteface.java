package com.example.urbantour2019.restapi;

import com.example.urbantour2019.ui.entity.Categoria;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriaInteface {

    @GET("categorias")
    Call<List<Categoria>> getCategorias();
}
