package com.example.urbantour2019.restapi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitApiAdapter {

    public Retrofit connectionEnable(){
        return new Retrofit.Builder()
                .baseUrl(BaseUrl.URL_WEB_SERVICES)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
